namespace Pichou

open System
open System.Threading.Tasks
open DSharpPlus
open DSharpPlus.CommandsNext
open DSharpPlus.CommandsNext.Attributes
open DSharpPlus.Entities

type Commands () =
    inherit BaseCommandModule ()

    [<Command "Ping">]
    [<Description "Show latency between you and Pichou">]
    member _.Ping (ctx: CommandContext) =
        task {
            do! ctx.TriggerTypingAsync()

            let emoji = DiscordEmoji.FromName(ctx.Client, ":ping_pong:").ToString()
            let message = sprintf "%s | %ims" emoji ctx.Client.Ping

            let! _ =
                message
                |> ctx.Channel.SendMessageAsync

            return ()
        } :> Task

    [<Command "MyAvatar"; Description "Show your avatar.">]
    member _.MyAvatar (ctx: CommandContext) =
        task {
            do! ctx.TriggerTypingAsync()

            let! _ =
                ctx.Channel.SendMessageAsync ctx.User.AvatarUrl

            return ()
        } :> Task

    [<Command "Avatar"; Description "Show the avatar of the targeted user.">]
    member _.Avatar (ctx: CommandContext, [<Description "The user that you want to see the avatar.">] user: DiscordUser) =
        task {
            do! ctx.TriggerTypingAsync()

            let! _ =
                ctx.Channel.SendMessageAsync user.AvatarUrl

            return ()
        } :> Task


open DSharpPlus.SlashCommands

type SlashCommands () =
    inherit ApplicationCommandModule ()

    [<SlashCommand("Ping", "Show latency between you and Pichou")>]
    member _.Ping (ctx: InteractionContext) =
        task {
            do! ctx.CreateResponseAsync (
                InteractionResponseType.DeferredChannelMessageWithSource
            )

            let emoji = DiscordEmoji.FromName(ctx.Client, ":ping_pong:").ToString()
            let message = sprintf "%s | %ims" emoji ctx.Client.Ping

            let! _ = ctx.EditResponseAsync((new DiscordWebhookBuilder()).WithContent(message))

            return ()
        } :> Task

    [<SlashCommand("Avatar", "Show the avatar of the targeted user.")>]
    member _.Avatar (ctx: InteractionContext, [<Option("User", "The user that you want to see the avatar.")>] user: DiscordUser) =
        task {
            do! ctx.CreateResponseAsync (
                InteractionResponseType.ChannelMessageWithSource,
                (new DiscordInteractionResponseBuilder()).WithContent(user.AvatarUrl)
            )
        } :> Task